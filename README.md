Purdue Aerial Robotics
======================

> Patrician: what does your robot do, sam

> bovril: it collects data about the surrounding environment, then discards it and drives into walls

([http://bash.org/?240849](http://bash.org/?240849))

##About
This is the code for the PARTIEEE 2015-16 flight controller.

##Documentation
Documentation is held within the `doc` folder.

##Useful Links

- [Git](http://git-scm.com)
- [Learn Git Branching](http://pcottle.github.io/learnGitBranching/) This is an excellent interactive git tutorial that covers all of git's major features
- [Pro Git](http://git-scm.com/book) This is a great book for learning how to use git.
- [Submitting Pull Requests](https://help.github.com/articles/using-pull-requests)
- [Github Help](http://help.github.com)
- [Git Ready](http://gitready.com/) This is a site with a bunch of useful git tips and tricks
- [Arduino Language Reference](http://arduino.cc/en/Reference/HomePage)

##Commit Message Guidelines
Writing good commit messages is very important. Please follow the following format. The first line of the commit message should be **50 or less characters** and give a concise description of what is included in the commit. The second line should be **blank**. The rest of the commit should be a description of what the commit is for and what was done. The body of the commit message should be wrapped to **72 characters**. A few additional tips for writing commit messages can be found [here](http://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message). A sample commit message can be seen below.

```
Commit message title (50 characters or less)

Commit description (wrapped to 72 characters)
Describe what issue is being fixed or what feature was added.

Example:
Fixed issue where the plane was not turning right.

Describe what was done.
Example:
Variable x was adjusted in file flight.c
```
