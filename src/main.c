// main.c

#include "Kernel/Kernel.h"
#include "Kernel/Comms.h"

task tasks[TASK_COUNT];

int main(void)
{
	initialize();
	tasks_run();
	return 0;
}

/**
 * Sets up everything.
 */
void initialize(void)
{
	tasks_initialize(TASKSCHEDULER_PERIOD);
}