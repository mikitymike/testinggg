// TaskScheduler.h

#ifndef KERNEL_TASKSCHEDULER_H
#define KERNEL_TASKSCHEDULER_H

#define TASKSCHEDULER_PERIOD 200

unsigned char TaskSchedulerTimerFlag;

typedef struct task
{
	unsigned long period; //how often should it tick?
	void (*TaskTickFunc)(int); // tick function to call
	int state; //used for state machine
	unsigned long timeElapsed;
	unsigned int priority; //priority
} task;

void tasks_initialize(unsigned long period);
void tasks_run(void);

#endif