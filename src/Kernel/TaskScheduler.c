// TaskScheduler.c
// Preemptive task scheduler.

#include <libopencm3/stm32/nvic.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>

#include "TaskScheduler.h"

/**
 * Sets up the timer used by the task scheduler.
 */
void tasks_initialize(unsigned long period)
{
	/* Enable TIM2 clock. */
	rcc_periph_clock_enable(RCC_TIM2);

	/* Enable TIM2 interrupt. */
	nvic_enable_irq(NVIC_TIM2_IRQ);

	/* Reset TIM2 peripheral. */
	timer_reset(TIM2);

	/* Timer global mode:
	 * - No divider
	 * - Alignment edge
	 * - Direction up
	 */
	timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT,
		       TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

	/* Reset prescaler value.
	 * Running the clock at 5kHz.
	 */
	/*
	 * On STM32F4 the timers are not running directly from pure APB1 or
	 * APB2 clock busses.  The APB1 and APB2 clocks used for timers might
	 * be the double of the APB1 and APB2 clocks.  This depends on the
	 * setting in DCKCFGR register. By default the behaviour is the
	 * following: If the Prescaler APBx is greater than 1 the derived timer
	 * APBx clocks will be double of the original APBx frequencies. Only if
	 * the APBx prescaler is set to 1 the derived timer APBx will equal the
	 * original APBx frequencies.
	 *
	 * In our case here the APB1 is devided by 4 system frequency and APB2
	 * divided by 2. This means APB1 timer will be 2 x APB1 and APB2 will
	 * be 2 x APB2. So when we try to calculate the prescaler value we have
	 * to use rcc_apb1_freqency * 2!!! 
	 *
	 * For additional information see reference manual for the stm32f4
	 * familiy of chips. Page 204 and 213
	 */
	timer_set_prescaler(TIM2, ((rcc_apb1_frequency * 2) / 10000));

	/* Enable preload. */
	timer_disable_preload(TIM2);

	/* Continous mode. */
	timer_continuous_mode(TIM2);

	/* Period (36kHz). */
	timer_set_period(TIM2, period);

	/* Disable outputs. */
	timer_disable_oc_output(TIM2, TIM_OC1);
	timer_disable_oc_output(TIM2, TIM_OC2);
	timer_disable_oc_output(TIM2, TIM_OC3);
	timer_disable_oc_output(TIM2, TIM_OC4);

	/* -- OC1 configuration -- */

	/* Configure global mode of line 1. */
	timer_disable_oc_clear(TIM2, TIM_OC1);
	timer_disable_oc_preload(TIM2, TIM_OC1);
	timer_set_oc_slow_mode(TIM2, TIM_OC1);
	timer_set_oc_mode(TIM2, TIM_OC1, TIM_OCM_FROZEN);

	/* Set the capture compare value for OC1. */
	timer_set_oc_value(TIM2, TIM_OC1, 1000);

	/* ---- */

	/* ARR reload enable. */
	timer_disable_preload(TIM2);

	/* Counter enable. */
	timer_enable_counter(TIM2);

	/* Enable commutation interrupt. */
	timer_enable_irq(TIM2, TIM_DIER_CC1IE);
}

void tasks_run(task *tasks, int taskCount)
{
	int highest = -1;
	int highestIndex = -1;
	
	while(1)
	{
		unsigned char[taskCount] runnableTasks;
		highest = -1;
		highestIndex = -1;
		for(i = 0; i < taskCount; i ++)
		{		
			runnableTasks[i] = (tasks[i].elapsedTime >= tasks[i].period);
			tasks[i].elapsedTime += TASKSCHEDULER_PERIOD;
		}
		for (int i = 0; i < taskCount; i++) 
		{
			if (runnableTasks[i]) 
			{
				//finds highest value
				if (tasks[i].priority > highest) 
				{
					highest = tasks[i].priority;
					highestIndex = i;
				}			
			}
		}
		//code below runs the actual task
		tasks[highestIndex].state = tasks[highestIndex].TickFct(tasks[highestIndex].state); 
		tasks[highestIndex].elapsedTime = 0;			
		TaskSchedulerTimerFlag = 0;
		while(!TaskSchedulerTimerFlag)
		{
			__WFI();
		}
	}
	return 0;
}

// Interrupt handler
void tim2_isr(void)
{
	TaskSchedulerTimerFlag = 1;
}