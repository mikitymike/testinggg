#include "Comms.h"

unsigned char commSendPacket(COMM_MAVLINK_PACKET packet) 
{

	return 1;
}


COMM_MAVLINK_PACKET commGeneratePacket(unsigned char payload_length,
		void *payload, unsigned char message_id)
{
	COMM_MAVLINK_PACKET packet;
	packet.header[0] = 0xFE;
	packet.header[1] = payload_length;
	packet.header[2] = 0; // TODO Packet Sequence?
	packet.header[3] = SYSTEM_ID;
	packet.header[4] = 0; // TODO Implement Sending component, possibly parameter.
	packet.header[5] = message_id;
	packet.payload = payload;
	packet.checksum = getCRC16(payload);
	return packet;
}

